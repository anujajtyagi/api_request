## add backend info
provider "aws" {
    region = "us-east-1"
}

terraform {

    required_providers {
      aws = {
        version = "~> 4"
      }
    }
    backend "s3" {
        bucket = "tfstate"
        key = "test/100182739541/us-east-1/terraform.tfstate"
        region = "us-east-1"
        dynamodb_table = "tflock"
        role_arn = "arn:aws:iam::100182739541:role/admin_role"
    }

}